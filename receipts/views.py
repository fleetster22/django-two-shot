from django.shortcuts import (
    render,
    redirect,
)
from django.contrib.auth.decorators import login_required
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm

# Create a view that will get all of the instances of the Receipt model and
# put them in the context for the template.


@login_required
def receipt_list(request):
    user = request.user
    receipts_list = Receipt.objects.filter(purchaser=user)
    context = {"receipts_list": receipts_list}

    return render(request, "receipts/list.html", context)


# user must be logged in to purchase
@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            # pauses the save until the author is added
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            # tells browser which page to go to after data is saved
            return redirect("home")

    else:
        form = ReceiptForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    user = request.user
    categories_list = ExpenseCategory.objects.filter(owner=user)
    context = {"categories_list": categories_list}

    return render(request, "receipts/category_list.html", context)


@login_required
def account_list(request):
    user = request.user
    accounts_list = Account.objects.filter(owner=user)
    context = {"accounts_list": accounts_list}

    return render(request, "receipts/account_list.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            # pauses the save until the owner is added
            new_cat = form.save(False)
            new_cat.owner = request.user
            new_cat.save()
            # tells browser which page to go to after data is saved
            return redirect("category_list")

    else:
        form = ExpenseCategoryForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/create_cat.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            # pauses the save until the owner is added
            new_acct = form.save(False)
            new_acct.owner = request.user
            new_acct.save()
            # tells browser which page to go to after data is saved
            return redirect("account_list")

    else:
        form = AccountForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/create_acct.html", context)
