from django.contrib import admin
from receipts.models import Account, Receipt, ExpenseCategory

# Register your models here.


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = ("name", "number")


@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = ("vendor", "total", "date")


class Category_ListAdmin(admin.ModelAdmin):
    list_display = ("name", "number")


@admin.register(ExpenseCategory)
class ExpensesCategoryAdmin(admin.ModelAdmin):
    list_display = ("name", "owner")
